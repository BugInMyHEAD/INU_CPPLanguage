#include "Rectangle.h"
#include <iostream>

using namespace std;

namespace CPPL111602c
{
	void Rectangle::draw() const
	{
		cout << "Rectangle Draw" << endl;
		Shape::draw();
	}

	void Rectangle::move(int x, int y)
	{
		this->x += x;
		this->y += y;
		cout << "사각형 이동점 : ( " << x << " ,  " << y << " )" << endl;
		cout << "변경된 시작점 : ( " << this->x << " , " << this->y << " )" << endl;
	}
}