#pragma once

#include "Shape.h"
#define _USE_MATH_DEFINES
#include <math.h>

namespace CPPL111602c
{
	class Ellipse : public Shape
	{
		int width, height;

	public:

		void setWidth(int w) { width = w; }
		void setHeight(int h) { height = h; }
		double getArea() const { return M_PI * width * height / 4; }

		virtual void draw() const;
		virtual void move(int x, int y);
	};
}