#pragma once

#include "Shape.h"

namespace CPPL111602c
{
	class Rectangle : public Shape
	{
		int width, height;

	public:

		void setWidth(int w) { width = w; }
		void setHeight(int h) { height = h; }
		double getArea() const { return width * height; }

		virtual void draw() const;
		virtual void move(int x, int y);
	};
}