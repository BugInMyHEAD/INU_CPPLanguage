#include <iostream>
#include "Rectangle.h"
#include "Ellipse.h"
#include "Triangle.h"

using namespace std;
using namespace CPPL111602c;

int main()
{
	Shape *p[3];
	p[0] = new Rectangle();
	p[1] = new Ellipse();
	p[2] = new Triangle();
	p[0]->setOrigin(3, 5);
	((Rectangle *)p[0])->setWidth(4);
	((Rectangle *)p[0])->setHeight(2);
	p[1]->setOrigin(3, 3);
	((Ellipse *)p[1])->setWidth(4);
	((Ellipse *)p[1])->setHeight(2);
	p[2]->setOrigin(2, 2);
	((Triangle *)p[2])->setWidth(4);
	((Triangle *)p[2])->setHeight(2);
	for (int i = 0; i<3; i++)
	{
		p[i]->draw();
		cout << "��  �� : " << p[i]->getArea() << endl;
		p[i]->move(2, 1);
	}
}
