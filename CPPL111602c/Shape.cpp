#include "Shape.h"
#include <iostream>

using namespace std;

namespace CPPL111602c
{
	void Shape::draw() const
	{
		cout << "시작점 : ( " << x << " , " << y << " )" << endl;
		cout << "면  적 : " << getArea() << endl;
	}
}