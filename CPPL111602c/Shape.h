#pragma once

namespace CPPL111602c
{
	class Shape
	{
	protected:

		int x, y;

	public:

		void setOrigin(int x, int y)
		{
			this->x = x;
			this->y = y;
		}

		virtual void draw() const;

		virtual void move(int x, int y) { setOrigin(this->x + x, this->y + y); }
		virtual double getArea() const = 0;
	};
}