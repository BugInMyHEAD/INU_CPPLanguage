#include <iostream>
#include "SavingAcct.h"
#include "CheckingAcct.h"

using namespace std;
using namespace CPPL110903c;

int main()
{
	BankAcct* sa = new SavingAcct(5000000);
	BankAcct* ca = new CheckingAcct(10000000);

	sa->deposit(100000);
	ca->deposit(150000);
	sa->withdraw(500000);
	ca->withdraw(500000);

	sa->print();
	cout << "이자액 : " << sa->getInterest() << endl;
	ca->print();
	cout << "이자액 : " << ca->getInterest() << endl;

	delete sa;
	delete ca;
}
