#pragma once

#include <string>
#include <iostream>

using namespace std;

namespace CPPL110903c
{
	class BankAcct
	{
		double money, rate, interest;
		string name;

	protected:

		BankAcct(const string& name = "", double m = 0) : name(name), money(m) { }

	public:

		virtual ~BankAcct() { };

		void deposit(double m) { money += m; }

		void withdraw(double m) { money -= m; }

		double getInterest() const { return money * rate; }

		void setRate(double r) { rate = r; }

		virtual void print() const
		{
			cout << "==================" << endl;
			cout << name << endl;
			cout << "==================" << endl;
			cout << "���ݾ� : " << cout.setf(ios::fixed) << money << endl;
		}
	};
}