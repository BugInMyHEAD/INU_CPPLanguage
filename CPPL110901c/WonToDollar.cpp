#include "WonToDollar.h"


namespace CPPL110901c
{
	WonToDollar::WonToDollar(double ratio)
		: Converter(ratio)
	{
	}

	WonToDollar::~WonToDollar()
	{
	}

	string WonToDollar::getSourceString() const
	{
		return "��";
	}

	string WonToDollar::getDestString() const
	{
		return "�޷�";
	}

	double WonToDollar::convert(double src) const
	{
		return src / ratio;
	}
}