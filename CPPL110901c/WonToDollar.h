#pragma once
#include "Converter.h"

namespace CPPL110901c
{
	class WonToDollar :
		public Converter
	{
	protected:

		string getSourceString() const;
		string getDestString() const;
		double convert(double src) const;

	public:

		WonToDollar(double ratio);
		virtual ~WonToDollar();
	};
}