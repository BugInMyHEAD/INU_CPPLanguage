// 091402c.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include <stdio.h>
#include <tchar.h>
#include <iostream>

using namespace std;

int sum(int, int);

int main()
{
	int n(0);
	cout << "끝 수를 입력하세요>";
	cin >> n;
	cout << "1에서 " << n << "까지의 합은 " << sum(1, n) << "입니다." << endl;

    return 0;
}

int sum(int a, int b)
{
	int retVal = 0;
	for (int i2 = a; i2 <= b; i2++)
	{
		retVal += i2;
	}

	return retVal;
}