#pragma once

#define _USE_MATH_DEFINES
#include <math.h>
#include <string>
#include <iostream>

namespace CPPL113003c
{
	using namespace std;

	class Circle
	{
		string name;
		int radius;

	public:

		virtual ~Circle() { }

		Circle(int radius = 1, const string& name = "") : radius(radius), name(name) { }

		friend ostream& operator<<(ostream& os, const Circle& circle);
		friend istream& operator>>(istream& is, Circle& circle);
	};

	ostream& operator<<(ostream& os, const Circle& circle)
	{
		os << "<반지름 " << circle.radius << "인 " << circle.name << ">";

		return os;
	}

	istream& operator>>(istream& is, Circle& circle)
	{
		cout << "반지름 >> ";
		is >> circle.radius;
		cout << "이름 >> ";
		is >> circle.name;

		return is;
	}
}
