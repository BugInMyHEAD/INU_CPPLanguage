#pragma once

namespace CPPL101001c
{
	class Accumulator
	{
		int value;

	public:
		Accumulator();
		Accumulator(int value);

		int getValue() const;
		Accumulator& add(int value);
	};
}