#include "Accumulator.h"

using namespace CPPL101001c;

Accumulator::Accumulator()
	: Accumulator(0)
{
}

Accumulator::Accumulator(int value)
	: value(value)
{
}

int Accumulator::getValue() const
{
	return value;
}

Accumulator& Accumulator::add(int value)
{
	this->value += value;

	return *this;
}
