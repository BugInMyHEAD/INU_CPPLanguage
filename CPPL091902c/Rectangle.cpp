#include "Rectangle.h"

Rectangle::Rectangle(int width, int height)
{
	this->width = width;
	this->height = height;
	this->area = width * height;
}

int Rectangle::getArea()
{
	return area;
}

int Rectangle::getWidth()
{
	return width;
}

int Rectangle::getHeight()
{
	return height;
}

// Deprecated: the class don't have setters, and the variable, 'area' is automatically calculated when the instance is created.
void Rectangle::computeArea()
{
	this->area = width * height;
}