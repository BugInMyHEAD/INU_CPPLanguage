#ifndef _EXP_H_
#define _EXP_H_


#include <cstdlib>

class Exp
{
	int base, exp;
	bool valueIsValid = false;
	int value;

public:
	Exp(int base, int exp)
	{
		if (0 == base && 0 == exp)
		{
			exit(1);
		}

		this->base = base;
		this->exp = exp;
	}

	Exp(int base)
	{
		this->base = base;
		this->exp = 1;
	}

	Exp()
	{
		this->base = 1;
		this->exp = 1;
	}

	int getExp()
	{
		return exp;
	}

	int getBase()
	{
		return base;
	}

	// Supports lazy evaluation
	int getValue();

	bool equals(Exp exp);
};


#endif
