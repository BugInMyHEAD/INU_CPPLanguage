#pragma once

#include "Point.h"

class PointManager
{
	Point * points;
	size_t size;
public:
	PointManager();
	~PointManager();
	PointManager(size_t n);
	void input();
	void show();
};