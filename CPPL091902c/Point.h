#pragma once

#include <string>

using namespace std;


class Point
{
	double x, y;

public:
	Point();
	Point(double x, double y);

	double getX();
	double getY();
	void setX(double x);
	void setY(double y);

	void showPoint();
};