#include "PointManager.h"

PointManager::PointManager()
{
	PointManager(0);
}

PointManager::~PointManager()
{
	delete[] points;
}

PointManager::PointManager(size_t size)
{
	points = new Point[size];
	this->size = size;
}

void PointManager::input()
{
}

void PointManager::show()
{
}
