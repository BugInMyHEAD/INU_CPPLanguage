#pragma once

class Rectangle
{
	int width, height;
	int area;

public:
	Rectangle(int width, int height);

	int getArea();
	int getWidth();
	int getHeight();

	// Deprecated. The class don't have setters, and the variable, 'area' is automatically calculated when the instance is created.
	void computeArea();
};