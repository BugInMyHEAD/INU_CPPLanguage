#include "Exp.h"

// Supports lazy evaluation
int Exp::getValue()
{
	if (!valueIsValid)
	{
		value = 1;
		for (auto i2 = 0; i2 < exp; i2++)
		{
			value *= base;
		}
	}

	return value;
}

bool Exp::equals(Exp exp)
{
	return getValue() == exp.getValue();
}
