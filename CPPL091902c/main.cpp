#include <iostream>
#include "Rectangle.h"
#include "Point.h"
#include "Exp.h"

using namespace std;


int main()
{
	Rectangle rectangles[] = { Rectangle(1, 1), Rectangle(2, 2) };
	for each (auto& var2 in rectangles)
	{
		cout << var2.getArea() << endl;
	}
	cout << endl;

	Point points[] = { Point(), Point(0.5, 0.5) };
	for each (auto& var2 in points)
	{
		var2.showPoint();
		cout << endl;
	}
	cout << endl;

	Exp a(3, 2);
	Exp b(9);
	Exp c;

	cout << a.getValue() << ' ' << b.getValue() << ' ' << c.getValue() << endl;
	cout << "a의 베이스 " << a.getBase() << ',' << "지수 " << a.getExp() << endl;

	if (a.equals(b))
		cout << "same" << endl;
	else
		cout << "not same" << endl;
	cout << endl;
}