#include <iostream>
#include <vector>
using namespace std;

void printVector(const vector<int>& v)
{
	auto n = v.size();
	// 벡터의 각 원소 출력
	for (auto& a2 : v)
	{
		cout << a2 << " ";
	}
}

double getAverage(const vector<int>& v)
{
	int sum = 0;
	auto n = v.size();
	// 벡터의 원소 개수 모두 더하기
	for (size_t i2 = 0; i2 < n; i2++)
	{
		sum += v[i2];
	}

	return (double)sum / n;
}

int main()
{
	vector<int> v;       // 벡터 객체 생성

	while (true)
	{
		cout << "정수 입력(0을 입력하면 종료)>>";

		int num;
		cin >> num;
		if (num == 0)
			return 0;     // 프로그램 종료

		v.push_back(num); //입력받은 수를 벡터에 저장

		printVector(v);   // 벡터의 모든 수 출력

		cout << "평균 = " << getAverage(v) << endl;
	}
}
