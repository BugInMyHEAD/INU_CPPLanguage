#include <iostream>
#include "Book.h"

using namespace std;
using namespace CPPL101002c;

int main()
{
	Book cpp;
	cpp.set("명품C++", 10000, 500);

	auto java = cpp;
	java.set("명품자바", 12000, 1200);

	auto haskell(cpp);
	haskell.set("명품하스켈", 56000, 800);

	cpp.show();
	java.show();
	haskell.show();

	Book a("명품 C++", 30000, 500), b("고품 C++", 30000, 500);
	if (a == 30000) cout << "정가 30000원" << endl; // price 비교
	if (a == "명품 C++") cout << "명품 C++ 입니다." << endl; // 책 title 비교
	if (a == b) cout << "두 책이 같은 책입니다." << endl; // title, price, pages 모두 비교
}
