#pragma once

//#include <string>

using namespace std;

namespace CPPL101002c
{
	class Book
	{
		char * title = new char[0];
		int price;
		int page;

	public:
		Book();
		Book(const Book& book);
		Book(char* title, int price, int page);
		virtual ~Book();

		Book& operator=(const Book& book);
		bool operator==(const Book& book) const;
		bool operator==(const string& title) const;
		bool operator==(int price) const;
		friend bool operator==(int price, const Book& book);

		void set(char* title, int price, int page);
		virtual void show() const;
	};

	bool operator==(int price, const Book& book);
}
