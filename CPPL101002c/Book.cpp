#include <iostream>
#include <cstring>
#include "Book.h"

using namespace std;

namespace CPPL101002c
{
	Book::Book()
		: Book("", 0, 0)
	{
	}

	Book::Book(const Book& book)
	{
		*this = book;
	}

	Book::Book(char* title, int price, int page)
	{
		set(title, price, page);
	}

	Book::~Book()
	{
		delete[] title;
	}

	Book& Book::operator=(const Book& book)
	{
		if (this != &book)
			set(book.title, book.price, book.price);

		return *this;
	}

	bool Book::operator==(const Book& book) const
	{
		return
			( *this == book.title )
			& ( *this == book.price )
			& ( this->page == book.page );
	}

	bool Book::operator==(const string& title) const
	{
		return 0 == strcmp(this->title, title.c_str());
	}

	bool Book::operator==(int price) const
	{
		return this->price == price;
	}

	void Book::set(char* title, int price, int page)
	{
		this->~Book();

		size_t bookTitleLen = strlen(title) + 1;
		this->title = new char[bookTitleLen];
		strcpy_s(this->title, bookTitleLen, title);

		this->price = price;
		this->page = page;
	}

	void Book::show() const
	{
		cout << title << " " << price << "��" << endl;
	}

	bool operator==(int price, const Book& book)
	{
		return book == price;
	}
}