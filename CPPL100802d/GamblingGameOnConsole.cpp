#include <iostream>
#include "GamblingGameOnConsole.h"

using namespace std;
using namespace CPPL100801d;
using namespace CPPL100802d;

void GamblingGameOnConsole::play()
{
	cout << "***** 갬블링 게임을 시작합니다. *****" << endl;

	gamblingGame = GamblingGame(2);
	auto players = gamblingGame.getPlayers();
	string s3;
	cout << "첫번째 선수 이름>>";
	getline(cin, s3);
	players[0] = Player(s3);
	cout << "두번째 선수 이름>>";
	getline(cin, s3);
	players[1] = Player(s3);

	for (;;)
	{
		auto phase = gamblingGame.getPhase();
		if (GamblingGame::NEXT_GAMBLE == phase)
		{
			cout << gamblingGame.getWhoseTurn().getName() << "<Enter>";
			string s5;
			getline(cin, s5);
			gamblingGame.proceed_NEXT_GAMBLE();
			auto picked = gamblingGame.getPicked();
			cout << "                      ";
			for (size_t i6 = 0; i6 < GamblingGame::NO_OF_PICKED; i6++)
			{
				cout << picked[i6] << "       ";
			}
			if (gamblingGame.isCoincident())
			{
				cout << gamblingGame.getWhoseTurn().getName() << "님 승리!!" << endl;
			}
			else
			{
				cout << "아쉽군요!" << endl;
			}
		}
		else if (GamblingGame::OVER == phase)
		{
			break;
		}
		else
		{
			exit(1);
		}
	}
}