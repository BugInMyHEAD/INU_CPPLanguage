#pragma once

#include "../CPPL100801d/Player.h"

using namespace CPPL100801d;

namespace CPPL100802d
{
	typedef int noOfPlayers_t;

	class GamblingGame
	{
	public:
		enum Phase
		{
			OVER, NEXT_GAMBLE
		};

		static const size_t NO_OF_PICKED = 3;

	private:
		Phase phase = NEXT_GAMBLE;
		noOfPlayers_t noOfPlayers;
		Player * players;
		noOfPlayers_t whoseTurnIdx = 0;
		int picked[NO_OF_PICKED];

		void copier(const GamblingGame& gamblingGame);

	public:
		GamblingGame();
		GamblingGame(const GamblingGame& gamblingGame);
		GamblingGame(noOfPlayers_t noOfPlayers);
		virtual ~GamblingGame();

		GamblingGame& operator=(const GamblingGame& gamblingGame);

		Phase getPhase() const;
		void proceed_NEXT_GAMBLE();
		noOfPlayers_t getNoOfPlayers() const;
		Player* getPlayers() const;
		Player& getWhoseTurn() const;
		const int* getPicked() const;
		bool isCoincident() const;
	};
}