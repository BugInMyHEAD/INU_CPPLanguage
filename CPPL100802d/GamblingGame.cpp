#include "GamblingGame.h"

#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )

using namespace CPPL100801d;
using namespace CPPL100802d;

void GamblingGame::copier(const GamblingGame& GamblingGame)
{
	phase = GamblingGame.phase;
	noOfPlayers = GamblingGame.noOfPlayers;
	whoseTurnIdx = GamblingGame.whoseTurnIdx;
	players = new Player[GamblingGame.noOfPlayers];
	for (noOfPlayers_t i2 = 0; i2 < noOfPlayers; i2++)
	{
		players[i2] = GamblingGame.players[i2];
	}
}

GamblingGame::GamblingGame()
	: GamblingGame(0)
{
}

GamblingGame::GamblingGame(const GamblingGame& GamblingGame)
{
	copier(GamblingGame);
}

GamblingGame::GamblingGame(noOfPlayers_t noOfPlayers)
	: noOfPlayers(noOfPlayers)
{
	players = new Player[noOfPlayers];
}

GamblingGame::~GamblingGame()
{
	delete[] players;
}

GamblingGame& GamblingGame::operator=(const GamblingGame& GamblingGame)
{
	if (this != &GamblingGame)
	{
		this->~GamblingGame();
		copier(GamblingGame);
	}

	return *this;
}

GamblingGame::Phase GamblingGame::getPhase() const
{
	return phase;
}

void GamblingGame::proceed_NEXT_GAMBLE()
{
	if (NEXT_GAMBLE != phase)
	{
		exit(1);
	}

	const int RAND_LIMIT = 3;

	for (noOfPlayers_t i2 = 0; i2 < ARRLEN(picked); i2++)
	{
		picked[i2] = rand() % RAND_LIMIT;
	}
	if (isCoincident())
	{
		phase = OVER;
	}

	whoseTurnIdx++;
	whoseTurnIdx %= noOfPlayers;
	phase = NEXT_GAMBLE;
}

noOfPlayers_t GamblingGame::getNoOfPlayers() const
{
	return noOfPlayers;
}

Player* GamblingGame::getPlayers() const
{
	return players;
}

Player& GamblingGame::getWhoseTurn() const
{
	return players[whoseTurnIdx];
}

const int* GamblingGame::getPicked() const
{
	return picked;
}

bool GamblingGame::isCoincident() const
{
	noOfPlayers_t i0 = 1;
	for (; i0 < ARRLEN(picked) && picked[0] == picked[i0]; i0++);
	if (ARRLEN(picked) == i0)
	{
		return true;
	}

	return false;
}
