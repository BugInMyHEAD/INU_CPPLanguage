#pragma once

#include "GamblingGame.h"

namespace CPPL100802d
{
	class GamblingGameOnConsole
	{
		GamblingGame gamblingGame;

	public:
		void play();
	};
}