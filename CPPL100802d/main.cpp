#include <iostream>
#include <ctime>
#include "GamblingGameOnConsole.h"

using namespace CPPL100802d;

int main()
{
	srand(time(nullptr));
	GamblingGameOnConsole gamblingGameOnConsole;
	gamblingGameOnConsole.play();
}