#define _USE_MATH_DEFINES

#include <iostream>
#include <cstring>
#include <cmath>
#include "Circle.h"

using namespace std;

namespace CPPL101801c
{
	int Circle::count = 0;

	int Circle::getCount()
	{
		return count;
	}

	void Circle::set(double radius, const char* color)
	{
		setRadius(radius);
		setColor(color);
	}

	void Circle::setColor(const char* color)
	{
		delete[] this->color;
		size_t size = strlen(color) + 1;
		this->color = new char[size];
		strcpy_s(this->color, size, color);
	}

	void Circle::deleter()
	{
		delete[] color;
	}

	Circle::~Circle()
	{
		deleter();
		count--;
	}

	Circle::Circle()
		: Circle(0, "")
	{
	}

	Circle::Circle(double radius, const char* color)
	{
		set(radius, color);
		count++;
	}

	Circle::Circle(const Circle& circle)
	{
		*this = circle;
		count++;
	}

	Circle::Circle(Circle&& circle)
	{
		*this = circle;
		count++;
	}

	Circle& Circle::operator=(const Circle& circle)
	{
		if (&circle != this)
		{
			set(circle.radius, circle.color);
		}
		
		return *this;
	}

	Circle& Circle::operator=(Circle&& circle)
	{
		if (&circle != this)
		{
			deleter();
			this->radius = circle.radius;
			this->color = circle.color;
			circle.color = nullptr;
		}

		return *this;
	}

	double Circle::getRadius() const
	{
		return radius;
	}

	void Circle::setRadius(double radius)
	{
		this->radius = radius;
	}

	double Circle::getArea() const
	{
		return M_PI * radius * radius;
	}

	double Circle::getPerimeter() const
	{
		return 2 * M_PI * radius;
	}

	void Circle::print() const
	{
		cout << radius << ", " << color << endl;
	}

	bool is_equal(const Circle& a, const Circle& b)
	{
		return a.getRadius() == b.getRadius();
	}
}