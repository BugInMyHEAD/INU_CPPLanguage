#pragma once

namespace CPPL101801c
{
	class Circle
	{
		static int count;

	public:
		static int getCount();

	private:
		double radius;
		char * color = new char[0];

		void set(double radius, const char* color);
		void setColor(const char* color);
		void deleter();

	public:
		virtual ~Circle();
		Circle();
		Circle(double radius, const char* color);
		Circle(const Circle& circle);
		Circle(Circle&& circle);

		Circle& operator=(const Circle& circle);
		Circle& operator=(Circle&& circle);

		double getRadius() const;
		void setRadius(double radius);
		double getArea() const;
		double getPerimeter() const;
		void print() const;
	};
	
	bool is_equal(const Circle& a, const Circle& b);
}