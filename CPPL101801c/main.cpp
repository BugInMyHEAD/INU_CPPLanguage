#include <iostream>
#include "Circle.h"

using namespace std;
using namespace CPPL101801c;

int main()
{
	Circle circleA;

	{
		Circle circleB(1, "Red");
		circleB.print();
		circleA = circleB;
		cout << is_equal(circleA, circleB) << endl;
		circleA = move(circleB);
		cout << is_equal(circleA, circleB) << endl;
	}
	// 'circleB' destructed

	circleA.print();
}
