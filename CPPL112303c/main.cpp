#include <iostream>

using namespace std;

template<typename T> struct Comparable
{
	virtual bool operator<(const T& t) const = 0;
	virtual bool operator>(const T& t) const = 0;
	virtual bool operator==(const T& t) const = 0;
};

class Circle : public Comparable<Circle>
{
	int radius;

public:

	Circle(int radius = 0) { this->radius = radius; }
	int getRadius() const { return radius; }

	bool operator<(const Circle& circle) const { return getRadius() < circle.getRadius(); }
	bool operator>(const Circle& circle) const { return getRadius() > circle.getRadius(); }
	bool operator==(const Circle& circle) const { return getRadius() == circle.getRadius(); }
};

template<class T> T bigger(T a, T b)
{ // 두 개의 매개 변수를 비교하여 큰 값을 리턴
	if (a > b) return a;
	else return b;
}

int main()
{
	int a = 20, b = 50, c;
	c = bigger(a, b);
	cout << "20과 50중 큰 값은 " << c << endl;
	Circle waffle(10), pizza(20), y;
	y = bigger(waffle, pizza);
	cout << "waffle과 pizza 중 큰 것의 반지름은 " << y.getRadius() << endl;
	waffle < pizza;
}
