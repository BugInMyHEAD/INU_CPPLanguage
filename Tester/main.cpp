#include <iostream>
#include <string>

using namespace std;

int main()
{
	cout.unsetf(ios::dec); // 10진수 해제
	cout.setf(ios::hex); // 16진수로 설정
	cout << 30 << endl; // 1e가 출력됨

	cout.setf(ios::dec | ios::showpoint); // 10진수 표현과 동시에 실수에 
										  // 소숫점이하 나머지는 0으로 출력
	cout << 23.5 << endl; // 23.5000으로 출력

}