#pragma once

#include "Person.h"

class PersonManager
{
	size_t size;
	Person * people;

public:
	PersonManager(size_t size);
	~PersonManager();

	void input();
	void show();
	void search();
};

