#include <iostream>
#include "Person.h"

using namespace std;

Person::Person()
{
	name = "";
	tel = "";
}

string Person::getName()
{
	return name;
}

string Person::getTel()
{
	return tel;
}

void Person::set(const string& name, const string& tel)
{
	this->name = name;
	this->tel = tel;
}