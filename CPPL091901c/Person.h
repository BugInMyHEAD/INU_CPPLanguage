#pragma once

using namespace std;

class Person
{
	string name;
	string tel;

public:
	Person();
	string getName();
	string getTel();
	void set(const string& name, const string& tel);
};