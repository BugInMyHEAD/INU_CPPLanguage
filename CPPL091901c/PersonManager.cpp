#include <iostream>
#include <string>
#include "PersonManager.h"

using namespace std;

PersonManager::PersonManager(size_t size)
{
	this->size = size;
	people = new Person[size];
}

PersonManager::~PersonManager()
{
	delete[] people;
}

void PersonManager::input()
{
	cout << "이름과 전화번호를 입력해주세요" << endl;
	for (size_t i2 = 0; i2 < size; i2++)
	{
		cout << "사람 " << i2 + 1 << ">> ";
		string name, tel;
		cin >> name >> tel;
		people[i2] = Person();
		people[i2].set(name, tel);
	}
}

void PersonManager::show()
{
	cout << "모든 사람의 이름은 ";
	for (size_t i2 = 0; i2 < size; i2++)
	{
		cout << people[i2].getName() << ' ';
	}
	cout << endl;
}

void PersonManager::search()
{
	cout << "전화번호 검색합니다. 이름을 입력하세요>>";
	string nameFromUser;
	cin >> nameFromUser;
	{
		size_t i1;
		for (i1 = 0; i1 < size; i1++)
		{
			if (people[i1].getName() == nameFromUser)
				break;
		}

		if (i1 < size)
		{
			cout << "전화 번호는 " << people[i1].getTel() << endl;
		}
		else
		{
			cout << "없는 사람입니다" << endl;
		}
	}
}
