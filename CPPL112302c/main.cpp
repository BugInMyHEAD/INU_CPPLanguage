#include <iostream>

using namespace std;

template<typename T> bool search(T target, const T* array, size_t size)
{
	for (size_t i2 = 0; i2 < size; i2++)
	{
		if (target == array[i2])
		{
			return true;
		}
	}

	return false;
}

int main()
{
	int x[] = { 1, 10, 99, 5, 4 };
	if (search(100, x, 5))  // 100이 배열 x에 포함되어 있는가?
		cout << "100이 배열 x에 포함되어 있다";
	else cout << "100이 배열 x에 포함되어 있지 않다";
	cout << endl;

	char c[] = { 'h', 'e', 'l', 'l', 'o' };
	if (search('e', c, 5))  // 'e'가 배열 c에 포함되어 있는가?
		cout << "e가 배열 c에 포함되어 있다";
	else cout << "e가 배열 c에 포함되어 있지 않다";
	cout << endl;
}
