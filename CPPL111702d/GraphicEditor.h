#pragma once

#include "Shape.h"

namespace CPPL111702d
{
	class GraphicEditor
	{
		Shape* pStart = nullptr, *pLast = nullptr;
		size_t noOfLine = 0, noOfCircle = 0, noOfRect = 0;

		// 각 도형의 종류별 개수 갱신
		void sizeAdd(const Shape* shape);
		void sizeSubtract(const Shape* shape);

	protected:

		void addFirst(Shape* shape);
		void addLast(Shape* shape);

		// index는 0부터 시작
		// deletable은 동적 할당되지 않은 Shape를 delete 하지 않기 위함
		void remove(size_t index, bool deletable = true);

	public:

		enum State
		{
			EXIT, MAIN_MENU, INSERT_MENU, REMOVE_MENU, SHOW_ALL_MENU, STATISTICS_MENU
		};

		virtual ~GraphicEditor();
		GraphicEditor() {}

		void run();

		size_t size() const { return noOfLine + noOfCircle + noOfRect; }
		size_t getNoOfLine() const { return noOfLine; }
		size_t getNoOfCircle() const { return noOfCircle; }
		size_t getNoOfRect() const { return noOfRect; }
	};
}