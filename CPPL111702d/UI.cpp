#include "UI.h"
#include <iostream>
#include "Line.h"
#include "Circle.h"
#include "Rect.h"

using namespace std;

namespace CPPL111702d
{
	void UI::printMainMenu()
	{
		cout << "삽입:1. 삭제:2. 모두보기:3. 통계정보:4. 종료:5 >> ";
	}

	GraphicEditor::State UI::inputMainMenu()
	{
		int intFromUser;
		cin >> intFromUser;
		switch (intFromUser)
		{
		case 1:
			return GraphicEditor::INSERT_MENU;
		case 2:
			return GraphicEditor::REMOVE_MENU;
		case 3:
			return GraphicEditor::SHOW_ALL_MENU;
		case 4:
			return GraphicEditor::STATISTICS_MENU;
		case 5:
			return GraphicEditor::EXIT;
		default: // 잘못 선택했을 경우 메인 메뉴 표시 위함
			return GraphicEditor::MAIN_MENU;
		}
	}

	void UI::printShowAllMenu(const Shape* shape)
	{
		for (auto i2 = 0; shape; i2++, shape = shape->getNext())
		{
			cout << i2 << ": ";
			shape->paint();
		}
	}

	void UI::printInsertMenu()
	{
		cout << "선:1. 원:2. 사각형:3 >> ";
	}

	Shape* UI::inputInsertMenu()
	{
		int intFromUser;
		cin >> intFromUser;
		switch (intFromUser)
		{
		case 1:
			return new Line;
		case 2:
			return new Circle;
		case 3:
			return new Rect;
		default:
			return nullptr;
		}
	}

	void UI::printRemoveMenu()
	{
		cout << "삭제하고자 하는 도형의 인덱스 >> ";
	}

	size_t UI::inputRemoveMenu()
	{
		size_t indexFromUser;
		cin >> indexFromUser;

		return indexFromUser;
	}

	void UI::printStatisticMenu(const GraphicEditor* graphicEditor)
	{
		cout << "선:" << graphicEditor->getNoOfLine()
			<< " 원:" << graphicEditor->getNoOfCircle()
			<< " 사각형" << graphicEditor->getNoOfRect()
			<< endl;
	}
}