#include "GraphicEditor.h"
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include "UI.h"
#include "Line.h"
#include "Circle.h"
#include "Rect.h"

using namespace std;

namespace CPPL111702d
{
	GraphicEditor::~GraphicEditor()
	{
		try
		{
			for (;;) remove(0); // 동적 할당된 'Shape'들 할당 해제
		}
		catch (out_of_range)
		{
		}
	}

	void GraphicEditor::run()
	{
		cout << "그래픽 에디터입니다." << endl;
		for (auto state = MAIN_MENU; ; )
		{
			switch (state)
			{
			case MAIN_MENU:
				UI::printMainMenu();
				state = UI::inputMainMenu();
				break;

			case INSERT_MENU:
				UI::printInsertMenu();
				addLast(UI::inputInsertMenu());
				state = MAIN_MENU;
				break;

			case REMOVE_MENU:
				UI::printRemoveMenu();
				try
				{
					remove(UI::inputRemoveMenu());
				}
				catch (out_of_range)
				{
					cout << "그 위치에는 도형이 없습니다." << endl;
				}
				state = MAIN_MENU;
				break;

			case SHOW_ALL_MENU:
				UI::printShowAllMenu(pStart);
				state = MAIN_MENU;
				break;

			case STATISTICS_MENU:
				UI::printStatisticMenu(this);
				state = MAIN_MENU;
				break;

			case EXIT:
				return;
			}
		}
	}

	void GraphicEditor::sizeAdd(const Shape* shape)
	{
		if (typeid(*shape) == typeid(Line))
		{
			noOfLine++;
		}
		else if (typeid(*shape) == typeid(Circle))
		{
			noOfCircle++;
		}
		else if (typeid(*shape) == typeid(Rect))
		{
			noOfRect++;
		}
	}

	void GraphicEditor::sizeSubtract(const Shape* shape)
	{
		if (typeid(*shape) == typeid(Line))
		{
			noOfLine--;
		}
		else if (typeid(*shape) == typeid(Circle))
		{
			noOfCircle--;
		}
		else if (typeid(*shape) == typeid(Rect))
		{
			noOfRect--;
		}
	}

	void GraphicEditor::addFirst(Shape* shape)
	{
		shape->add(pStart); // nullptr를 받으면 exception
		pStart = shape;
		if (!pLast) pLast = shape;
		sizeAdd(shape);
	}

	void GraphicEditor::addLast(Shape* shape)
	{
		if (!shape) // nullptr를 받으면 error
			throw invalid_argument("cannot receive a nullptr by 'shape'");

		if (pLast) pLast->add(shape);
		pLast = shape;
		if (!pStart) pStart = shape;
		sizeAdd(shape);
	}

	void GraphicEditor::remove(size_t index, bool deletable)
	{
		if (size() <= index) // 존재하지 않는 것을 지우려 하면 error
			throw out_of_range("_size <= index");

		Shape * pBeingRemoved = nullptr;
		Shape * pPrevious = nullptr;

		if (index)
		{
			pPrevious = pStart;
			// pPrevious가 index=0인 Shape를 가리키므로 index=1부터 찾음
			for (auto i4 = 1; i4 < index; i4++) 
			{
				pPrevious = pPrevious->getNext();
			}
			pBeingRemoved = pPrevious->cut();
			pPrevious->add(pBeingRemoved->getNext());
		}
		else // index=0일 때 pStart를 갱신해야 함
		{
			pBeingRemoved = pStart;
			pStart = pBeingRemoved->getNext();
		}
		if (pBeingRemoved == pLast) pLast = pPrevious;
		sizeSubtract(pBeingRemoved);

		if (deletable) delete pBeingRemoved; // 동적 할당된 것만 delete
	}
}