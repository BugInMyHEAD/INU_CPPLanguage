#pragma once

#include "Shape.h"

namespace CPPL111702d
{
	class Rect : public Shape
	{
	public:

		virtual void paint() const;
	};
}