#pragma once

namespace CPPL111702d
{
	class Shape
	{
		Shape* next = nullptr;

	public:

		virtual ~Shape() {}
		Shape() {}

		Shape* getNext() const { return next; }

		Shape* add(Shape* p); // returns the pointer of last 'Shape' of 'p'(Shape*)'s list
		Shape* cut(); // cuts off the connection with 'next'(Shape*) and returns 'next'
		virtual void paint() const = 0;
	};
}