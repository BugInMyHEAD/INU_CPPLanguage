#pragma once

#include "Shape.h"

namespace CPPL111702d
{
	class Line : public Shape
	{
	public:

		virtual void paint() const;
	};
}