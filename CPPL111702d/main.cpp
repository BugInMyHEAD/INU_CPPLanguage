#include <string>
#include <iostream>
#include "Circle.h"
#include "Rect.h"
#include "Line.h"
#include "GraphicEditor.h"

using namespace std;
using namespace CPPL111702d;

int main()
{
	GraphicEditor* ge = new GraphicEditor();
	ge->run();

	delete ge;
}