#pragma once

#include "Shape.h"

namespace CPPL111702d
{
	class Circle : public Shape
	{
	public:

		virtual void paint() const;
	};
}