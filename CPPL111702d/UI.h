#pragma once

#include "GraphicEditor.h"

namespace CPPL111702d
{
	class UI
	{
	public:

		static void printMainMenu();
		static GraphicEditor::State inputMainMenu();

		static void printInsertMenu();
		static Shape* inputInsertMenu();

		static void printRemoveMenu();
		static size_t inputRemoveMenu();

		static void printShowAllMenu(const Shape* shape);

		static void printStatisticMenu(const GraphicEditor* graphicEditor);
	};
}