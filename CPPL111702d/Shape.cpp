#include <iostream>
#include "Shape.h"

using namespace std;

namespace CPPL111702d
{
	void Shape::paint() const
	{
		cout << "--Shape--" << endl;
	}

	Shape* Shape::add(Shape* p)
	{
		auto disconnected = next; // 끊어진 부분
		next = p;
		auto pEnd = this;
		while (pEnd->next) pEnd = pEnd->next; // 다시 이어야 하는, 끝 부분의 'Shape'를 찾음
		pEnd->next = disconnected; // 이음

		return pEnd;
	}

	Shape* Shape::cut()
	{
		auto retVal = next;
		next = nullptr;

		return retVal;
	}
}