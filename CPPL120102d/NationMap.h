#pragma once

#include <map>
#include "../CPPL120101d/Container.h"
#include "../CPPL120101d/Nation.h"

namespace CPPL120102d
{
	using namespace CPPL120101d;

	// Nation의 나라이름을 key로, 수도명을 value로 나누어 map에 저장
	class NationMap : public MyContainer<Nation>
	{
		map<string, string> m;

	public:

		virtual ~NationMap() {}

		virtual size_t size() const
		{
			return m.size();
		}

		virtual Nation at(size_t idx) const
		{
			// map의 iterator는 bidirectional iterator이기 때문에 인덱스로 바로 접근할 수 없고 필요한 만큼 증감 연산해야 한다
			auto tempIt = m.begin();
			for (size_t i2 = 0; i2 < idx; i2++)
			{
				tempIt++;
			}
			auto pair = *tempIt;

			// first에 나라이름, second에 수도명
			return Nation(pair.first, pair.second);
		}

		virtual bool add(const Nation& nation)
		{
			// key를 나라이름으로 쓰고 있으므로 프로그래머가 따로 검색할 필요 없이 다음의 함수 사용
			auto pair = m.emplace(nation.getName(), nation.getCapital());

			// second에 할당이 실제로 일어났는지(key가 중복되지 않았는지)가 들어있다
			return pair.second;
		}

		virtual void modify(const Nation& nation)
		{
			try
			{
				m.at(nation.getName()) = nation.getCapital();
			}
			catch (out_of_range) {} // 해당하는 나라이름이 없으면 아무 동작도 하지 않음
		}
	};
}