#include "../CPPL120101d/CapitalGameOnConsole.h"
#include "NationMap.h"

using namespace CPPL120101d;
using namespace CPPL120102d;

int main()
{
	CapitalGameOnConsole<NationMap> capitalGameOnConsole;
	capitalGameOnConsole.run();
}