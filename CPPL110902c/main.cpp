#include <iostream>
#include <string>

#include "AND_OR_XOR.h"

using namespace std;
using namespace CPPL110902c;

int main()
{
	ANDGate and;
	ORGate or ;
	XORGate xor;

	and.set(true, false);
	or .set(true, false);
	xor.set(true, false);
	AbstractGate *ap = &and;
	cout.setf(ios::boolalpha); // 불린 값을 "true”,"false" 로 출력할 것을 지시
	cout << ap->operation() << endl; // AND 결과는 false
	ap = &or ;
	cout << ap->operation() << endl; // OR 결과는 true
	ap = &xor;
	cout << ap->operation() << endl; // XOR 결과는 true
}