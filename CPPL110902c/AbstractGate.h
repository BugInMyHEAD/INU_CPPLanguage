#pragma once

namespace CPPL110902c
{
	class AbstractGate
	{
	protected:

		bool x, y;

	public:

		void set(bool x, bool y)
		{
			this->x = x;
			this->y = y;
		}

		virtual bool operation() const = 0; // 순수 가상 함수
	};
}