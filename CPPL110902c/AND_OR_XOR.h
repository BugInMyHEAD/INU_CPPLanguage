#pragma once

#include "AbstractGate.h"

namespace CPPL110902c
{
	class ANDGate :
		public AbstractGate
	{
	public:
		bool operation() const
		{
			return x && y;
		}
	};

	class ORGate : public AbstractGate
	{
	public:
		bool operation() const
		{
			return x || y;
		}
	};

	class XORGate : public AbstractGate
	{
	public:
		bool operation() const
		{
			return x ^ y;
		}
	};
}