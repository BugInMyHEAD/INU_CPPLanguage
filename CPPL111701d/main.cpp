#include <iostream>
#include <string>
#include <conio.h>
#include "InkJetPrinter.h"
#include "LaserPrinter.h"
#include "LinePrinter.h"
#include "../customdef.h"

using namespace std;
using namespace CPPL111701h;

int main()
{
	InkJetPrinter ink("Officejet V40", "HP", 5, 10);
	LaserPrinter laser("SCX-6x45", "삼성전자", 3, 20);
	LinePrinter line("KP7203HZT", "Printronix", 1, 30);
	Printer * printer[] = { &ink, &laser, &line }; // 업캐스팅
	string hangulName[] = { "잉크젯", "레이저", "라인" };

	// 프린터 상태 출력
	cout << "현재 작동중인 3 대의 프린터는 아래와 같다" << endl;
	for (auto i2 = 0; i2 < ARRLEN(printer); i2++)
	{
		cout << hangulName[i2] << " : ";
		printer[i2]->show();
		cout << endl;
	}
	cout << endl;

	for (;;)
	{
		const int userPrinterSelectOffset = 1; // 사용자의 입력은 0부터가 아니라 1부터임

		// 프린터 종류와 인덱스 출력
		cout << "프린터(";
		for (int i4 = 0; i4 < ARRLEN(printer); i4++)
		{
			if (i4)
			{
				cout << ". ";
			}
			cout << i4 + 1 << ":" << hangulName[i4];
		}
		cout << ")와 매수 입력 >> ";

		// 사용자로부터 사용할 프린터와 인쇄할 매수 입력받음
		int printerNum, page;
		cin >> printerNum;
		cin >> page;

		// 인쇄 후 결과 출력
		printerNum -= userPrinterSelectOffset;
		if (0 < printerNum && printerNum < ARRLEN(printer))
		{
			printer[printerNum]->print(page);
		}
		for (auto i4 = 0; i4 < ARRLEN(printer); i4++)
		{
			printer[i4]->show();
			cout << endl;
		}

		// 인쇄를 계속할지 사용자에게 물어봄
		cout << "계속 프린트 하시겠습니까(y/n)>>";
		for (;;)
		{
			int yn = _getch();
			cout << (char)yn << endl;
			switch (toupper(yn))
			{
			case 'N':
				goto __no_more_print__;
			case 'Y':
				break;
			default:
				continue;
			}

			break;
		}
		cout << endl;
	}

__no_more_print__:;
}