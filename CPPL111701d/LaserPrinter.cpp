#include <iostream>
#include "LaserPrinter.h"

using namespace std;

namespace CPPL111701h
{
	LaserPrinter::LaserPrinter(const string& model, const string& manufacturer, int availableCount, int availableToner)
		: Printer(model, manufacturer, availableCount), availableToner(availableToner)
	{
	}

	void LaserPrinter::show() const
	{
		Printer::show();
		cout << " .남은 토너 " << availableToner;
	}

	int LaserPrinter::print(int pages, int lack)
	{
		if (pages < availableToner)
		{
			if (!Printer::print(pages, lack)) // 부족한 자원 없음
			{
				availableToner -= pages; // 자원 사용
			}
		}
		else
		{
			Printer::print(pages, lack + 1);
			cout << "토너가 부족하여 프린트할 수 없습니다." << endl;
		}

		return lack;
	}

	void LaserPrinter::addAvailableToner(int pages) // 종이 보충
	{
		auto i0 = pages + availableToner;

		// 'pages + availableToner'가 음수이면 overflow_error 예외 던짐
		if (i0 < 0)
			throw overflow_error("pages + availableToner < 0");

		availableToner = i0;
	}
}