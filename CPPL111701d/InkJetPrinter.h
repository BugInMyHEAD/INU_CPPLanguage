#pragma once

#include "Printer.h"

namespace CPPL111701h
{
	class InkJetPrinter : public Printer
	{
		int availableInk;

	public:

		InkJetPrinter(const std::string& model = "", const std::string& manufacturer = "", int availableCount = 0, int availableInk = 0);

		virtual void show() const;

		void addAvailableInk(int pages); // ��ũ ����

	protected:

		virtual int print(int pages, int lack);
	};
}