#include <stdexcept>
#include <iostream>
#include "Printer.h"

using namespace std;

namespace CPPL111701h
{
	Printer::Printer(const string& model, const string& manufacturer, int availableCount)
		: model(model), manufacturer(manufacturer), availableCount(availableCount)
	{
	}

	void Printer::print(int pages)
	{
		// 'pages'가 음수이면 invalid_argument 예외 던짐
		if (pages <= 0)
			throw invalid_argument("pages must be greater than 0");

		print(pages, 0);
	}

	int Printer::print(int pages, int lack) // 'lack'은 부족한 자원을 세어서 부모 print 함수에 넘겨주기 위한 매개변수
	{
		if (!lack && pages <= availableCount)
		{
			printedCount += pages;
			availableCount -= pages;
			cout << "프린트하였습니다.";
		}
		else
		{
			cout << "용지가 부족하여 프린트할 수 없습니다.";
		}
		cout << endl;

		return lack; // 부족한 자원의 수 반환하여 자식 print 함수가 자원을 사용해야 하는지 알 수 있게 함
	}

	void Printer::show() const
	{
		cout << model
			<< " ." << manufacturer
			<< " .남은 종이 " << availableCount << "장";
	}

	void Printer::addAvailableCount(int pages) // 종이 보충
	{
		auto i0 = pages + availableCount;

		// 'pages + availableCount'가 음수이면 overflow_error 예외 던짐
		if (i0 < 0)
			throw overflow_error("pages + availableCount < 0");

		availableCount = i0;
	}
}