#include <iostream>
#include "LinePrinter.h"

using namespace std;

namespace CPPL111701h
{
	LinePrinter::LinePrinter(const string& model, const string& manufacturer, int availableCount, int availableDrum)
		: Printer(model, manufacturer, availableCount), availableDrum(availableDrum)
	{
	}

	void LinePrinter::show() const
	{
		Printer::show();
		cout << " .남은 드럼 " << availableDrum;
	}

	int LinePrinter::print(int pages, int lack)
	{
		if (pages < availableDrum)
		{
			if (!Printer::print(pages, lack)) // 부족한 자원 없음
			{
				availableDrum -= pages; // 자원 사용
			}
		}
		else
		{
			Printer::print(pages, lack + 1);
			cout << "드럼이 부족하여 프린트할 수 없습니다." << endl;
		}

		return lack;
	}

	void LinePrinter::addAvailableDrum(int pages) // 종이 보충
	{
		auto i0 = pages + availableDrum;

		// 'pages + availableDrum'가 음수이면 overflow_error 예외 던짐
		if (i0 < 0)
			throw overflow_error("pages + availableDrum < 0");

		availableDrum = i0;
	}
}