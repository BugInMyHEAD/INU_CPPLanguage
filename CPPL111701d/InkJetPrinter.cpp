#include <iostream>
#include "InkJetPrinter.h"

using namespace std;

namespace CPPL111701h
{
	InkJetPrinter::InkJetPrinter(const string& model, const string& manufacturer, int availableCount, int availableInk)
		: Printer(model, manufacturer, availableCount), availableInk(availableInk)
	{
	}

	void InkJetPrinter::show() const
	{
		Printer::show();
		cout << " .남은 잉크 " << availableInk;
	}

	int InkJetPrinter::print(int pages, int lack)
	{
		if (pages < availableInk)
		{
			if (!Printer::print(pages, lack)) // 부족한 자원 없음
			{
				availableInk -= pages; // 자원 사용
			}
		}
		else
		{
			Printer::print(pages, lack + 1);
			cout << "잉크가 부족하여 프린트할 수 없습니다." << endl;
		}

		return lack;
	}

	void InkJetPrinter::addAvailableInk(int pages) // 종이 보충
	{
		auto i0 = pages + availableInk;

		// 'pages + availableInk'가 음수이면 overflow_error 예외 던짐
		if (i0 < 0)
			throw overflow_error("pages + availableInk < 0");

		availableInk = i0;
	}
}