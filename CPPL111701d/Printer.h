#pragma once

#include <string>

namespace CPPL111701h
{
	class Printer
	{
		std::string model, manufacturer; // 모델명, 제조사명
		int printedCount = 0; // 인쇄된 종이 매수
		int availableCount; // 남아 있는 종이 매수

	public:

		virtual ~Printer() { };

		Printer(const std::string& model = "", const std::string& manufacturer = "", int availableCount = 0);

		void print(int pages);

		virtual void show() const;

		void addAvailableCount(int pages); // 종이 보충

	protected:

		// 'lack'은 부족한 자원을 세어서 부모 print 함수에 넘겨주기 위한 매개변수
		// 부족한 자원의 수 반환하여 자식 print 함수가 자원을 사용해야 하는지 알 수 있게 함
		virtual int print(int pages, int lack);
	};
}