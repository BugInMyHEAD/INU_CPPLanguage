#pragma once

#include "Printer.h"

namespace CPPL111701h
{
	class LinePrinter : public Printer
	{
		int availableDrum;

	public:

		LinePrinter(const std::string& model = "", const std::string& manufacturer = "", int availableCount = 0, int availableDrum = 0);

		virtual void show() const;

		void addAvailableDrum(int pages); // �巳 ����

	protected:

		virtual int print(int pages, int lack);
	};
}