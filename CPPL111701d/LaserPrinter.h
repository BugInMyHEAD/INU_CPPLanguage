#pragma once

#include "Printer.h"

namespace CPPL111701h
{
	class LaserPrinter : public Printer
	{
		int availableToner;

	public:

		LaserPrinter(const std::string& model = "", const std::string& manufacturer = "", int availableCount = 0, int availableToner = 0);

		virtual void show() const;

		void addAvailableToner(int pages); // ��� ����

	protected:

		virtual int print(int pages, int lack);
	};
}