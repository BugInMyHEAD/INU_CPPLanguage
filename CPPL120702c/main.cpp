#include <iostream>

using namespace std;

int get() throw( char* )
{
	int n;

	cout << "0~9 사이의 정수 입력 >> ";
	cin >> n;
	char c;
	c = cin.get();
	if ('\n' != getchar() || n < 0 || 9 < n)
	{
		throw "input fault";
	}

	return n;
}

int main()
{
	int n, m;
	while (true)
	{
		try
		{
			n = get();
			m = get();
			cout << n << 'x' << m << '=' << n*m << endl;
		}
		catch (char* s)
		{
			cout << s << " 예외 발생. 계산할 수 없음" << endl;
		}
	}
}
