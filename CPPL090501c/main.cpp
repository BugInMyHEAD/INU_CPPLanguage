// main.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

double area(int r);

int main()
{
	int n(3);
	char c = '#';
	cout << c << 5.5 << '-' << n << "hello" << true << endl;
	cout << "n + 5 = " << n + 5 << '\n';
	cout << "면적은 " << area(n);
	
	return 0;
}

double area(int r)
{
	return M_PI * r * r;
}