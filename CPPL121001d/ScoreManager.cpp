#include "ScoreManager.h"
#include <fstream>
#include <iomanip>

using namespace std;

namespace CPPL121001d
{
	bool ScoreManager::inputStudent(string filename) throw ( string )
	{
		auto retVal = true;

		// 중복 라인을 저장하는 vector 초기화
		duplicatedLine.clear();

		// filestream 열고 유효한지 확인
		ifstream fin;
		fin.open(filename);
		if (!fin)
		{
			throw "파일 읽기 실패";
		}

		// eof가 아닌 동안 학번과 이름을 읽어서 map에 저장
		for (int i2 = 1; !fin.eof(); i2++)
		{
			string idString;
			string name;
			getline(fin, idString, ';');
			getline(fin, name);
			int id = atoi(idString.c_str());
			StudentScore ss(id, name);
			// emplace는 중복되면 할당하지 않고 pair의 second에 false를 대입하여 반환한다
			auto pair = ssMap.emplace(ss.getId(), ss);
			// 중복되면 해당 라인숫자 저장 및 반환값 저장
			if (!pair.second)
			{
				duplicatedLine.push_back(i2);
				retVal = false;
			}
		}

		fin.close();

		return retVal;
	}

	StudentScore& ScoreManager::findStudent(int id) throw( out_of_range )
	{
		return ssMap.at(id);
	}

	void ScoreManager::outputStudent(string filename) const throw ( string )
	{
		// filestream 열고 유효한지 확인
		ofstream fout;
		fout.open(filename);
		if (!fout)
		{
			throw "파일 쓰기 실패";
		}

		fout << studentScoreField << endl;
		for (auto& var2 : ssMap)
		{
			fout << var2.second << endl;
		}

		fout.close();
	}

	ostream& operator<<(ostream& os, const StudentScore& ss)
	{
		os << setw(8) << ss.getId();
		os << setw(8) << ss.getName();
		os << setw(8) << ss.getScore(StudentScore::Subject::KOR);
		os << setw(8) << ss.getScore(StudentScore::Subject::ENG);
		os << setw(8) << ss.getScore(StudentScore::Subject::MAT);
		os << setw(8) << ss.getAverage();

		return os;
	}

	ostream& studentScoreField(ostream& os)
	{
		os << setw(8) << "   학번";
		os << setw(8) << "   이름";
		os << setw(8) << "   국어";
		os << setw(8) << "   영어";
		os << setw(8) << "   수학";
		os << setw(8) << "   평균";

		return os;
	}
}