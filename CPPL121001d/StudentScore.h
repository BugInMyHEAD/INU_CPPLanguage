#pragma once

#include <string>
#include "../customdef.h"

namespace CPPL121001d
{
	using namespace std;

	class StudentScore
	{
		int id;
		string name;
		int score[3];

	public:

		virtual ~StudentScore() { }

		StudentScore(int id, const string& name);

		enum class Subject { KOR, ENG, MAT };
		const size_t NUM_OF_SUBJECT = ARRLEN(score);

		int getId() const { return id; }
		string getName() const { return name; }

		int getScore(Subject subject) const { return score[(int)subject]; }
		void setScore(Subject subject, int subjectScore) { score[(int)subject] = subjectScore; }

		int getTotal() const;
		double getAverage() const;
	};
}