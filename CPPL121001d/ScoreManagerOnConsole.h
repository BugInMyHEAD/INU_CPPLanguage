#pragma once

#include "ScoreManager.h"

namespace CPPL121001d
{
	using namespace std;

	class ScoreManagerOnConsole
	{
		ScoreManager sm;

		friend istream& operator>>(istream& is, StudentScore& ss);
		friend ostream& operator<<(ostream& os, const StudentScore& ss);

	public:

		void run();
	};

	istream& operator>>(istream& is, StudentScore& ss);
	ostream& id(ostream& os);
}