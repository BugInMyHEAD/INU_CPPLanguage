#include "StudentScore.h"

namespace CPPL121001d
{
	StudentScore::StudentScore(int id, const string& name)
		: id(id), name(name)
	{
		for (size_t i2 = 0; i2 < NUM_OF_SUBJECT; i2++)
		{
			score[i2] = -1;
		}
	}

	int StudentScore::getTotal() const
	{
		int retVal;

		retVal = 0;
		for (size_t i2 = 0; i2 < NUM_OF_SUBJECT; i2++)
		{
			retVal += score[i2];
		}

		return retVal;
	}

	double StudentScore::getAverage() const
	{
		return (double)getTotal() / NUM_OF_SUBJECT;
	}
}