#pragma once

#include <map>
#include <vector>
#include "StudentScore.h"

namespace CPPL121001d
{
	using namespace std;

	class ScoreManager
	{
		map<int, StudentScore> ssMap;
		vector<int> duplicatedLine;
		string field = "   학번        이름      국어    영어    수학    평균";

	public:

		virtual ~ScoreManager() { }

		ScoreManager() { }

		bool inputStudent(string filename) throw ( string );
		StudentScore& findStudent(int id) throw( out_of_range );
		void outputStudent(string filename) const throw ( string );
		vector<int>::const_iterator getDuplications() const
		{
			return duplicatedLine.cbegin();
		}
		vector<int>::const_iterator getDuplicationsEnd() const
		{
			return duplicatedLine.end();
		}
		string getField() const { return field; }

		friend ostream& operator<<(ostream& os, const StudentScore& ss);
	};


	ostream& operator<<(ostream& os, const StudentScore& ss);
	ostream& studentScoreField(ostream& os);
}
