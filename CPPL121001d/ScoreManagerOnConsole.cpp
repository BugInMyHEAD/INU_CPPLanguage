#include "ScoreManagerOnConsole.h"
#include <iostream>
#include <conio.h>
#include <cctype>

namespace CPPL121001d
{
	void ScoreManagerOnConsole::run()
	{
		for (;;)
		{
			cout << "1.학생정보입력.  2.성적입력.  3.개인성적조회.  4.성적파일출력.  5.종료" << endl;
			cout.flush();
			int ch3 = _getch();
			string inputString;
			vector<int>::const_iterator it;
			StudentScore * ss;
			switch (ch3)
			{
			case '1':
				//cout << "파일이름:";
				//getline(cin, inputString);
				try
				{
					//if (!sm.inputStudent(inputString))
					if (!sm.inputStudent("std_info.txt"))
					{
						// 중복이 감지되면
						it = sm.getDuplications();
						cout << "다음 라인이 중복됨:" << endl;

						while (it != sm.getDuplicationsEnd())
						{
							cout << *it++ << endl;
						}
					}
				}
				catch (string excep2)
				{
					cout << excep2 << endl;
				}
				break;

			case '2':
				try
				{
					cout << id;
					getline(cin, inputString);
					cin >> sm.findStudent(atoi(inputString.c_str()));
				}
				catch (out_of_range)
				{
					cout << "해당 학번이 없음" << endl;
				}
				break;

			case '3':
				try
				{
					cout << id;
					getline(cin, inputString);
					ss = &sm.findStudent(atoi(inputString.c_str()));
					cout << studentScoreField << endl;
					cout << *ss << endl;
				}
				catch (out_of_range)
				{
					cout << "해당 학번이 없음" << endl;
				}
				break;

			case '4':
				//cout << "파일이름:";
				//getline(cin, inputString);
				try
				{
					//sm.outputStudent(inputString);
					sm.outputStudent("std_score.txt");
				}
				catch (string excep2)
				{
					cout << excep2 << endl;
				}
				break;

			case '5':
				return;
			}
		}
	}

	istream& operator>>(istream& is, StudentScore& ss)
	{
		for (size_t i2 = 0; i2 < ss.NUM_OF_SUBJECT; i2++)
		{
			if (ss.getScore(( StudentScore::Subject )i2) >= 0)
			{
				// 음수이면 수정하지 않은 상태이므로
				for (;;)
				{
					cout << "기존 성적이 있습니다. 계속 수정하시겠습니까? (y/n)" << endl;
					switch (tolower(_getch()))
					{
					case 'n':
						return is;
					case 'y':
						goto __modify;
					}
				}
			}
		}

__modify:
		int score;
		cout << "국어 점수:";
		is >> score;
		ss.setScore(StudentScore::Subject::KOR, score);
		cout << "영어 점수:";
		is >> score;
		ss.setScore(StudentScore::Subject::ENG, score);
		cout << "수학 점수:";
		is >> score;
		ss.setScore(StudentScore::Subject::MAT, score);
		is.ignore();

		return is;
	}

	ostream& id(ostream& os) { return os << "학번:"; }
}