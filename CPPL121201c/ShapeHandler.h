#pragma once

#include "Shape.h"
#include "Rectangle.h"
#include "Ellipse.h"
#include "Triangle.h"

namespace CPPL121201c
{
	extern int ShId;

	class ShapeHandler
	{
		Shape * p[100];
		int ShNum = 0;

	public:

		virtual ~ShapeHandler();

		void ShowMenu() const;
		void MakeShape();
		void MoveShape();
		void DrawShape() const;
		void AreaShape() const;
		void ShowAllShInfo() const;

	protected:

		void MakeRectangle();
		void MakeEllipse();
		void MakeTriangle();
	};
}