#pragma once

#include "Shape.h"

namespace CPPL121201c
{
	class Triangle : public Shape
	{
		int width, height;

	public:

		virtual ~Triangle() { }

		Triangle(int ShID, int x, int y, int w, int h) : Shape(ShID, x, y), width(w), height(h) { }

		double GetArea() const { return width * height / 2; }
		void Draw() const;
	};
}