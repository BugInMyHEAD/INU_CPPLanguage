#pragma once

namespace CPPL121201c
{
	class Shape
	{
		int ShID;
		int x, y;

	public:

		virtual ~Shape() { }
		Shape(int ShID, int x, int y) : ShID(ShID), x(x), y(y) {}

		int GetShID() const { return ShID; }
		virtual double GetArea() const = 0;
		virtual void Draw() const = 0;
		void Move(int x, int y) { this->x += x; this->y += y; }
		void ShowShInfo() const;
	};
}