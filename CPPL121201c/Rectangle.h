#pragma once

#include "Shape.h"

namespace CPPL121201c
{
	class Rectangle : public Shape
	{
		int width, height;

	public:

		virtual ~Rectangle() { }

		Rectangle(int ShID, int x, int y, int w, int h) : Shape(ShID, x, y), width(w), height(h) { }

		double GetArea() const { return width * height; }
		void Draw() const;
	};
}