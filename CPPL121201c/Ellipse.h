#pragma once

#define _USE_MATH_DEFINES
#include <math.h>
#include "Shape.h"

namespace CPPL121201c
{
	class Ellipse : public Shape
	{
		int width, height;

	public:

		virtual ~Ellipse() { }

		Ellipse(int ShID, int x, int y, int w, int h) : Shape(ShID, x, y), width(w), height(h) { }

		double GetArea() const { return width * height * M_PI / 4; }
		void Draw() const;
	};
}