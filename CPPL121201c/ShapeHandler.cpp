#include "ShapeHandler.h"
#include <iostream>

using namespace std;

namespace CPPL121201c
{
	int ShId = 101;

	ShapeHandler::~ShapeHandler()
	{
		while (--ShNum > 0)
		{
			delete p[ShNum];
		}
	}

	void ShapeHandler::ShowMenu() const
	{
		cout << "-----Menu------" << endl
			<< "1. 도형 만들기" << endl
			<< "2. 도형 이  동" << endl
			<< "3. 도형 그리기" << endl
			<< "4. 면적 구하기" << endl
			<< "5. 모든도형출력" << endl
			<< "6. 프로그램 종료" << endl
			<< "선택: ";
	}

	void ShapeHandler::MakeShape()
	{
		cout << "[도형종류 선택]" << endl
			<< "1. 사각형 2. 타원형 3. 삼각형" << endl
			<< "선택: ";
		int i0;
		cin >> i0;
		cin.clear();
		switch (i0)
		{
		case 1:
			MakeRectangle();
			break;
		case 2:
			MakeEllipse();
			break;
		case 3:
			MakeTriangle();
			break;
		}
		ShId++;
		ShNum++;
		cout << endl;
	}

	void ShapeHandler::MoveShape()
	{
		cout << "[이    동]" << endl
			<< "도형ID: ";
		int id = -1;
		cin >> id;
		cin.clear();
		{
			int i2;
			for (i2 = 0; i2 < ShNum && id != p[i2]->GetShID(); i2++);
			if (i2 >= ShNum)
			{
				cout << "해당 ID가 없습니다." << endl;
			}
			else
			{
				int x, y;
				cout << "x: ";
				cin >> x;
				cout << "y: ";
				cin >> y;
				p[i2]->Move(x, y);
				p[i2]->ShowShInfo();
				cout << " : 이동완료" << endl;
			}
		}
		cout << endl;
	}

	void ShapeHandler::DrawShape() const
	{
		cout << "도형ID: ";
		int id = -1;
		cin >> id;
		cin.clear();

		{
			int i2;
			for (i2 = 0; i2 < ShNum && id != p[i2]->GetShID(); i2++);
			if (i2 >= ShNum)
			{
				cout << "해당 ID가 없습니다." << endl;
			}
			else
			{
				p[i2]->ShowShInfo();
				cout << endl;
				p[i2]->Draw();
				cout << endl;
			}
		}
		cout << endl;
	}

	void ShapeHandler::AreaShape() const
	{
		cout << "[면    적]" << endl
			<< "도형ID: ";
		int id = -1;
		cin >> id;
		cin.clear();

		{
			int i2;
			for (i2 = 0; i2 < ShNum && id != p[i2]->GetShID(); i2++);
			if (i2 >= ShNum)
			{
				cout << "해당 ID가 없습니다." << endl;
			}
			else
			{
				cout << "면적은 " << p[i2]->GetArea() << "입니다.면적완료" << endl;
			}
		}
		cout << endl;
	}

	void ShapeHandler::ShowAllShInfo() const
	{
		for (int i1 = 0; i1 < ShNum; i1++)
		{
			cout << "도형ID: " << p[i1]->GetShID() << endl;
			p[i1]->ShowShInfo();
			cout << endl;
			p[i1]->Draw();
			cout << endl << endl;
		}
	}

	void ShapeHandler::MakeRectangle()
	{
		int x, y, w, h;
		cout << "[사각형 만들기]" << endl
			<< "도형 ID: " << ShId << endl;
		cout << "x 좌표: ";
		cin >> x;
		cin.clear();
		cout << "y 좌표: ";
		cin >> y;
		cin.clear();
		cout << "가 로: ";
		cin >> w;
		cin.clear();
		cout << "세 로: ";
		cin >> h;
		cin.clear();
		p[ShNum] = new Rectangle(ShId, x, y, w, h);
	}

	void ShapeHandler::MakeEllipse()
	{
		int x, y, w, h;
		cout << "[타원형 만들기]" << endl
			<< "도형 ID: " << ShId << endl;
		cout << "x 좌표: ";
		cin >> x;
		cin.clear();
		cout << "y 좌표: ";
		cin >> y;
		cin.clear();
		cout << "가 로: ";
		cin >> w;
		cin.clear();
		cout << "세 로: ";
		cin >> h;
		cin.clear();
		p[ShNum] = new Ellipse(ShId, x, y, w, h);
	}

	void ShapeHandler::MakeTriangle()
	{
		int x, y, w, h;
		cout << "[삼각형 만들기]" << endl
			<< "도형 ID: " << ShId << endl;
		cout << "x 좌표: ";
		cin >> x;
		cin.clear();
		cout << "y 좌표: ";
		cin >> y;
		cin.clear();
		cout << "가 로: ";
		cin >> w;
		cin.clear();
		cout << "세 로: ";
		cin >> h;
		cin.clear();
		p[ShNum] = new Triangle(ShId, x, y, w, h);
	}
}
