#include "ShapeHandler.h"
#include <iostream>

using namespace std;
using namespace CPPL121201c;

int main()
{
	ShapeHandler shapeHandler;
	for (;;)
	{
		shapeHandler.ShowMenu();

		int i0;
		cin >> i0;
		cout << endl;

		switch (i0)
		{
		case 1:
			shapeHandler.MakeShape();
			break;
		case 2:
			shapeHandler.MoveShape();
			break;
		case 3:
			shapeHandler.DrawShape();
			break;
		case 4:
			shapeHandler.AreaShape();
			break;
		case 5:
			shapeHandler.ShowAllShInfo();
			break;
		case 6:
			goto __main_exit;
		}
	}
__main_exit:;
}