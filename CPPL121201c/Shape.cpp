#include "Shape.h"
#include <iostream>
#include <string>
#include "Ellipse.h"
#include "Rectangle.h"
#include "Triangle.h"

using namespace std;

namespace CPPL121201c
{
	void Shape::ShowShInfo() const
	{
		cout << "<<" << x << ", " << y << ">>";
	}
}
