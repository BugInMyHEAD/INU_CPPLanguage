#include "CapitalGameOnConsole.h"
#include "NationVector.h"

using namespace CPPL120101d;

int main()
{
	CapitalGameOnConsole<NationVector> capitalGameOnConsole;
	capitalGameOnConsole.run();
}