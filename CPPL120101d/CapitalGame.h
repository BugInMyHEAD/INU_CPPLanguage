#pragma once

#include <ctime>
#include <type_traits>
#include "Nation.h"
#include "Container.h"

namespace CPPL120101d
{
	template<typename T> class CapitalGame
	{
		static_assert(is_convertible_v<T, MyContainer<Nation>>, "Type variable T must be a Container<Nation>");

	public:

		enum Phase
		{
			EXIT, MAIN, INSERT, MODIFY, QUIZ_Q, QUIZ_A
		};

	private:
		Phase phase = MAIN; // 게임의 양상
		int correct = 0; // 맞춘 문제 수
		int question = 0; // 총 문제 수
		size_t quizIdx = -1; // 랜덤한 퀴즈 문제를 내기 위한 인덱스
		MyContainer<Nation> * container = new T(); // 위의 static_assert 문을 사용한 이유

	public:

		CapitalGame();

		virtual ~CapitalGame()
		{
			delete container;
		}

		Phase getState() const { return phase; }
		void when_main(Phase state);
		void when_insertInsert(const Nation& nation);
		void when_insertEnd();
		void when_modify(const Nation& nation);
		string when_quiz_q();
		bool when_quiz_a(const string& capital);
		void when_quizQuit();
		int getCorrect() const { return correct; }
		int getQuestion() const { return question; }
	};

	template<typename T> CapitalGame<T>::CapitalGame()
	{
		srand((unsigned)time(nullptr)); // 랜덤 문제를 위해

		container->add(Nation("한국", "서울"));
		container->add(Nation("네팔", "카트만두"));
		container->add(Nation("동티모르", "딜리"));
		container->add(Nation("라오스", "비엔티안"));
		container->add(Nation("레바논", "베이루트"));
		container->add(Nation("말레이시아", "콸라룸푸르"));
		container->add(Nation("몰디브", "말레"));
		container->add(Nation("몽골", "울란바토르"));
		container->add(Nation("미얀마", "네피도"));
		container->add(Nation("바레인", "마나마"));
	}

	// 메뉴 선택
	template<typename T> void CapitalGame<T>::when_main(Phase phase)
	{
		if (MAIN != this->phase)
		{
			throw domain_error("MAIN != state");
		}

		this->phase = phase;

		if (QUIZ_Q == phase)
		{
			correct = question = 0; // 맞춘 문제 수, 총 문제 수 초기화
		}
	}

	// 정보 입력
	template<typename T> void CapitalGame<T>::when_insertInsert(const Nation& nation)
	{
		if (INSERT != phase)
		{
			throw domain_error("INSERT != state");
		}

		if (!container->add(nation))
		{
			cout << "already exists" << endl;
		}
	}

	// 정보 입력 종료
	template<typename T> void CapitalGame<T>::when_insertEnd()
	{
		if (INSERT != phase)
		{
			throw domain_error("INSERT != state");
		}

		phase = MAIN;
	}

	// 정보 수정
	template<typename T> void CapitalGame<T>::when_modify(const Nation& nation)
	{
		if (MODIFY != phase)
		{
			throw domain_error("MODIFY != state");
		}

		container->modify(nation);

		phase = MAIN;
	}

	// 퀴즈 문제 생성
	template<typename T> string CapitalGame<T>::when_quiz_q()
	{
		if (QUIZ_Q != phase)
		{
			throw domain_error("QUIZ_Q != state");
		}

		quizIdx = rand() % container->size();

		phase = QUIZ_A; // 답변 받기

		return container->at(quizIdx).getName();
	}

	// 퀴즈 답 평가
	template<typename T> bool CapitalGame<T>::when_quiz_a(const string& capital)
	{
		if (QUIZ_A != phase)
		{
			throw domain_error("QUIZ_A != state");
		}

		bool retVal;
		question++;
		if (container->at(quizIdx).getCapital() == capital)
		{
			correct++;

			retVal = true;
		}
		else
		{
			retVal = false;
		}

		phase = QUIZ_Q; // 계속 문제 내기

		return retVal;
	}

	// 퀴즈 종료
	template<typename T> void CapitalGame<T>::when_quizQuit()
	{
		if (QUIZ_Q != phase && QUIZ_A != phase)
		{
			throw domain_error("QUIZ_Q != state && QUIZ_A != state");
		}

		quizIdx = -1; // 퀴즈 인덱스 초기화

		phase = MAIN;
	}
}
