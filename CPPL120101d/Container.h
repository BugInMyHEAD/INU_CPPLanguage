#pragma once

namespace CPPL120101d
{
	using namespace std;

	// vector와 map에 각각 접근하기 위한 인터페이스
	template<typename T> class MyContainer
	{
	public:

		virtual ~MyContainer() { }

		virtual size_t size() const = 0;
		virtual T at(size_t) const = 0; // 퀴즈문제를 랜덤으로 내기 위해
		virtual bool add(const T&) = 0; // 정보입력
		virtual void modify(const T&) = 0; // 정보수정
	};
}