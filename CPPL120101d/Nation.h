#pragma once

#include <string>

namespace CPPL120101d
{
	using namespace std;

	class Nation
	{
		string name, capital;

	public:
		virtual ~Nation() {}

		Nation(const string& name, const string& capital)
		{
			setName(name);
			setCapital(capital);
		}

		string getName() const { return name; }
		void setName(const string& name) { this->name = name; }

		string getCapital() const { return capital; }
		void setCapital(const string& capital) { this->capital = capital; }
	};
}