#pragma once

#include <vector>
//#include <iterator>
#include "Container.h"
#include "Nation.h"

namespace CPPL120101d
{
	using namespace std;

	// Nation을 vector에 저장
	class NationVector : public MyContainer<Nation>
	{
		vector<Nation> v;

	public:

		virtual ~NationVector() {}

		virtual size_t size() const
		{
			return v.size();
		}

		virtual Nation at(size_t idx) const
		{
			return v[idx];
		}

		virtual bool add(const Nation& nation)
		{
			// vector의 모든 요소를 검색하여 같은 나라이름을 가진 Nation 객체가 있으면
			for (auto& elem2 : v)
			{
				if (elem2.getName() == nation.getName())
				{
					return false;
				}
			}

			v.push_back(nation); // 마지막에 저장

			return true;
		}

		virtual void modify(const Nation& nation)
		{
			// vector의 모든 요소를 검색하여 같은 나라이름을 가진 Nation 객체의 수도명을 수정
			for (auto& elem2 : v)
			{
				if (elem2.getName() == nation.getName())
				{
					elem2.setCapital(nation.getCapital());

					break;
				}
			}
		}
	};
}