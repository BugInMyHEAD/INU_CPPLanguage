#pragma once

#include "CapitalGame.h"
#include <iostream>

namespace CPPL120101d
{
	using namespace std;

	template<typename T> class CapitalGameOnConsole
	{
		CapitalGame<T> game;

	public:

		virtual ~CapitalGameOnConsole() {}

		void run();
	};

	template<typename T> void CapitalGameOnConsole<T>::run()
	{
		for (;;)
		{
			int i3;
			CapitalGame<T>::Phase phase;
			string name, capital;
			switch (game.getState())
			{
			case CapitalGame<T>::EXIT:
				return;

			case CapitalGame<T>::MAIN:
				cout << "1.정보입력, 2.정보수정, 3.퀴즈, 4.종료" << endl;
				cin >> i3;
				switch (i3)
				{
				case 1:
					phase = CapitalGame<T>::INSERT;
					break;
				case 2:
					phase = CapitalGame<T>::MODIFY;
					break;
				case 3:
					phase = CapitalGame<T>::QUIZ_Q;
					break;
				case 4:
					phase = CapitalGame<T>::EXIT;
					break;
				default:
					break;
				}
				game.when_main(phase);
				break;

			case CapitalGame<T>::INSERT:
				cout << "나라이름 수도명:";
				cin >> name;
				if ("no" == name) // no를 받으면 더 이상의 입력 종료
				{
					game.when_insertEnd();

					continue;
				}
				cin >> capital;
				game.when_insertInsert(Nation(name, capital));
				break;

			case CapitalGame<T>::MODIFY:
				cout << "나라이름 변경할수도명:";
				cin >> name;
				cin >> capital;
				game.when_modify(Nation(name, capital));
				break;

			case CapitalGame<T>::QUIZ_Q:
				cout << game.when_quiz_q() << ":";
				break;

			case CapitalGame<T>::QUIZ_A:
				cin >> capital;
				if ("q" == capital) // q를 받으면 퀴즈 종료
				{
					cout << game.getCorrect() << " / " << game.getQuestion() << endl;
					game.when_quizQuit();
				}
				else if (game.when_quiz_a(capital))
				{
					cout << "correct" << endl;
				}
				else
				{
					cout << "wrong" << endl;
				}
				break;
			}
		}
	}
}
