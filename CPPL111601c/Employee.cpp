#include <iostream>
#include "Employee.h"

using namespace std;

namespace CPPL111601c
{
	void Employee::display() const
	{
		Person::display();
		cout << this->getNumber() << endl;
		cout << this->getSalary() << endl;
		cout << compute_pay() << endl;
	};
}