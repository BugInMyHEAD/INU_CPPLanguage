#pragma once

#include <string>

namespace CPPL111601c
{
	class Person
	{
		std::string name;
		int age;
		bool gender;

	public:

		Person(const std::string& n = "", int a = 0, bool g = true) : name(n), age(a), gender(g) { }

		void setName(const std::string& n) { name = n; }
		void setName(const char* n) { name = n; }
		std::string getName() const { return name; }

		void setAge(int a) { age = a; }
		int getAge() const { return age; }

		void setGender(bool g) { gender = g; }
		bool getGender() const { return gender; }

		virtual void display() const;
		virtual int compute_pay() const { return 0; }
	};
}