#include "Person.h"
#include <iostream>

using namespace std;

namespace CPPL111601c
{
	void Person::display() const
	{
		cout << this->getName() << endl;
		cout << this->getAge() << endl;
		cout << this->getGender() << endl;
	}
}