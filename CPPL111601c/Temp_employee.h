#pragma once

#include <iostream>
#include "Person.h"

namespace CPPL111601c
{
	class Temp_employee : public Person
	{
		int number;
		int salary;

	public:
		Temp_employee(const std::string& n = "", int a = 0, bool g = true, int num = 0, int s = 0) : Person(n, a, g), number(num), salary(s) {}

		void setNumber(int n) { number = n; }
		int getNumber() const { return number; }

		void setSalary(int s) { salary = s; }
		int getSalary() const { return salary; }

		virtual void display() const;
		virtual int compute_pay() const { return salary * 25; }
	};
}