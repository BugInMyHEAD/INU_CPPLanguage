#include "Temp_employee.h"
#include <iostream>

using namespace std;

namespace CPPL111601c
{
	void Temp_employee::display() const
	{
		Person::display();
		cout << this->getNumber() << endl;
		cout << this->getSalary() << endl;
		cout << compute_pay() << endl;
	}
}