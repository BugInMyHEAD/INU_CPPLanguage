#include "Student.h"

namespace CPPL111403c
{

	Student::Student(const std::string& name, const std::string& phoneNo)
	{
		setName(name);
		setPhoneNo(phoneNo);
	}

	void Student::setName(const std::string& name)
	{
		this->name = name;
	}
	void Student::setPhoneNo(const std::string& phoneNo)
	{
		this->phoneNo = phoneNo;
	}
}