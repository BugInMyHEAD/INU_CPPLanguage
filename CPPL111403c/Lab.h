#pragma once

#include <string>
#include "Student.h"

namespace CPPL111403c
{
	class Lab
	{
		std::string name;
		Student * chief, * manager;

	public:
		virtual ~Lab() {}

		Lab(const std::string& name = "")
			: name(name)
		{
		}

		void setChief(Student* student);
		void setManager(Student* student);

		void print() const;
	};
}