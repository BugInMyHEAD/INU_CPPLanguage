#include "Lab.h"
#include <iostream>

using namespace std;

namespace CPPL111403c
{
	void Lab::setChief(Student* student)
	{
		chief = student;
	}

	void Lab::setManager(Student* student)
	{
		manager = student;
	}

	void Lab::print() const
	{
		cout << name << " 연구실" << endl
			<< "실장은 " << chief->getName() << endl
			<< "총무는 " << manager->getName() << endl;
	}
}