#pragma once

#include <string>

namespace CPPL111403c
{
	class Student
	{
		std::string name, phoneNo;

	public:

		virtual ~Student() {  }
		Student(const std::string& name, const std::string& phoneNo);

		std::string getName() const { return name; }
		void setName(const std::string& name);

		std::string getPhoneNo() const { return phoneNo; }
		void setPhoneNo(const std::string& phoneNo);
	};
}