#pragma once

#include <iostream>
#include <stdexcept>
#include "NamedCircle.h"

using namespace std;

namespace CPPL110201c
{
	class CircleManager
	{
		NamedCircle * namedCircles;
		size_t _size;
		
	public:
		virtual ~CircleManager()
		{
			delete[] namedCircles;
		}
		
		CircleManager(size_t size = 0)
			: _size(size), namedCircles(new NamedCircle[size])
		{
		}
		
		NamedCircle& biggest() const
		{
			if (0 == _size)
			{
				throw domain_error("There is no namedCircle");
			}

			NamedCircle * biggestCircle = &namedCircles[0];
			for (size_t i2 = 1; i2 < _size; i2++)
			{
				if (biggestCircle->getArea() < namedCircles[i2].getArea())
				{
					biggestCircle = &namedCircles[i2];
				}
			}

			return *biggestCircle;
		}
		
		void allshow() const
		{
			for (size_t i2 = 0; i2 < _size; i2++)
			{
				cout << i2 + 1 << "> "
					<< namedCircles[i2].getRadius() << " " << namedCircles[i2].getName()
					<< endl;
			}
		}

		size_t size() const
		{
			return _size;
		}

		NamedCircle * getNamedCircles() const
		{
			return namedCircles;
		}
	};
}