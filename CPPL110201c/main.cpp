#include <iostream>
#include <string>
#include "CircleManager.h"

using namespace std;
using namespace CPPL110201c;

int main()
{
	CircleManager circleManager(5);
	size_t circleManagerSize = circleManager.size();
	cout << circleManagerSize << " 개의 정수 반지름과 원의 이름을 입력하세요" << endl;

	NamedCircle * namedCircles = circleManager.getNamedCircles();
	for (size_t i2 = 0; i2 < circleManagerSize; i2++)
	{
		cout << i2 + 1 << ">> ";
		int radius;
		cin >> radius;
		namedCircles[i2].setRadius(radius);

		string name;
		cin >> name;
		namedCircles[i2].setName(name);
	}

	circleManager.allshow();

	cout << "가장 면적이 큰 피자는 "
		<< circleManager.biggest().getName() << "입니다" << endl;
}