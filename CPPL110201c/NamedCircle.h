#pragma once

#include <string>
#include <iostream>
#include "Circle.h"

using namespace std;

namespace CPPL110201c
{
	class NamedCircle : public Circle
	{
		string name;

	public:
		virtual ~NamedCircle()
		{
		}

		NamedCircle(int radius = 0, const string& name = "")
			: Circle(radius)
		{
			setName(name);
		}

		void show() const
		{
			cout << "반지름이 " << getRadius() << "인 " << name << endl;
		}

		const string& getName() const
		{
			return name;
		}

		void setName(string name)
		{
			this->name = name;
		}
	};
}