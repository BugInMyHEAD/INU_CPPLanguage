#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

namespace CPPL110201c
{
	class Circle
	{
		int radius;

	public:
		virtual ~Circle()
		{
		}

		Circle(int radius = 0)
		{
			setRadius(radius);
		}

		int getRadius() const
		{
			return radius;
		}

		void setRadius(int radius)
		{
			this->radius = radius;
		}

		double getArea() const
		{
			return M_PI * radius * radius;
		}
	};
}