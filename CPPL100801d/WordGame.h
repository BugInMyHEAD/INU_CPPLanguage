#pragma once

#include "Player.h"

namespace CPPL100801d
{
	typedef int noOfPlayers_t;

	class WordGame
	{
	public:
		enum Phase
		{
			OVER, NEXT_CHAIN
		};

	private:
		Phase phase = NEXT_CHAIN;
		noOfPlayers_t noOfPlayers;
		Player * players;
		noOfPlayers_t whoseTurnIdx = 0;
		string lastWord;

		void copier(const WordGame& wordGame);

	public:
		WordGame();
		WordGame(const WordGame& wordGame);
		WordGame(noOfPlayers_t noOfPlayers, const string& startingWord);
		virtual ~WordGame();

		WordGame& operator=(const WordGame& wordGame);

		Phase getPhase() const;
		void proceed_NEXT_CHAIN(const string& word);
		noOfPlayers_t getNoOfPlayers() const;
		Player* getPlayers() const;
		Player& getWhoseTurn() const;
		const string& getLastWord() const;
	};
}