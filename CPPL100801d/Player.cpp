#include "Player.h"

using namespace CPPL100801d;

Player::Player()
{
}

Player::Player(const string& name)
	: name(name)
{
}

const string& Player::getName() const
{
	return name;
}