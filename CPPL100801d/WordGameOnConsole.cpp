#include <iostream>
#include "WordGameOnConsole.h"

using namespace std;
using namespace CPPL100801d;

void WordGameOnConsole::play()
{
	cout << "끝말 잇기 게임을 시작합니다" << endl
		<< "게임에 참가하는 인원은 몇 명입니까? ";
	noOfPlayers_t noOfPlayers;
	cin >> noOfPlayers;
	wordGame = WordGame(noOfPlayers, "아버지");
	
	auto players = wordGame.getPlayers();
	cin.ignore();
	for (noOfPlayers_t i2 = 0; i2 < noOfPlayers; i2++)
	{
		cout << "참가자의 이름을 입력하세요.>>";
		string s3;
		getline(cin, s3);
		players[i2] = Player(s3);
	}
	
	cout << "시작하는 단어는 " << wordGame.getLastWord() << "입니다." << endl;
	for (;;)
	{
		auto phase = wordGame.getPhase();
		if (WordGame::NEXT_CHAIN == phase)
		{
			cout << wordGame.getWhoseTurn().getName() << ">>";
			string s5;
			getline(cin, s5);
			wordGame.proceed_NEXT_CHAIN(s5);
		}
		else if (WordGame::OVER == phase)
		{
			cout
				<< wordGame.getWhoseTurn().getName()
				<< "가(이) 졌습니다."
				<< endl;
			break;
		}
		else
		{
			exit(1);
		}
	}
}