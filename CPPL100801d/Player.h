#pragma once

#include <string>

using namespace std;

namespace CPPL100801d
{
	class Player
	{
		string name;
		
	public:
		Player();
		Player(const string& name);

		const string& getName() const;
	};
}