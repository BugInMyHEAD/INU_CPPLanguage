#include "WordGame.h"

using namespace CPPL100801d;

void WordGame::copier(const WordGame& wordGame)
{
	phase = wordGame.phase;
	noOfPlayers = wordGame.noOfPlayers;
	whoseTurnIdx = wordGame.whoseTurnIdx;
	lastWord = wordGame.lastWord;
	players = new Player[wordGame.noOfPlayers];
	for (noOfPlayers_t i2 = 0; i2 < noOfPlayers; i2++)
	{
		players[i2] = wordGame.players[i2];
	}
}

WordGame::WordGame()
	: WordGame(0, "�ѱ�")
{
}

WordGame::WordGame(const WordGame& wordGame)
{
	copier(wordGame);
}

WordGame::WordGame(noOfPlayers_t noOfPlayers, const string& startingWord)
	: noOfPlayers(noOfPlayers), lastWord(startingWord)
{
	players = new Player[noOfPlayers];
}

WordGame::~WordGame()
{
	delete[] players;
}

WordGame& WordGame::operator=(const WordGame& wordGame)
{
	if (this != &wordGame)
	{
		this->~WordGame();
		copier(wordGame);
	}

	return *this;
}

WordGame::Phase WordGame::getPhase() const
{
	return phase;
}

void WordGame::proceed_NEXT_CHAIN(const string& word)
{
	if (NEXT_CHAIN != phase)
	{
		exit(1);
	}

	if (word.size() < 2) // when the 'word' is too short.
	{
		phase = OVER;
	}
	else if (lastWord.at(lastWord.size() - 2) == word.at(0)
		&& lastWord.at(lastWord.size() - 1) == word.at(1))
	{
		whoseTurnIdx++;
		whoseTurnIdx %= noOfPlayers;
		lastWord = word;
		phase = NEXT_CHAIN;
	}
	else
	{
		phase = OVER;
	}
}

noOfPlayers_t WordGame::getNoOfPlayers() const
{
	return noOfPlayers;
}

Player* WordGame::getPlayers() const
{
	return players;
}

Player& WordGame::getWhoseTurn() const
{
	return players[whoseTurnIdx];
}

const string& WordGame::getLastWord() const
{
	return lastWord;
}