#pragma once

#include "Time.h"

namespace CPPL111401c
{
	class AlarmClock
	{
		Time current;	// 현재시각
		Time alarm;	// 알람시각

	public:

		AlarmClock(Time alarm, Time current)
			: alarm(alarm), current(current)
		{
		};

		void print() const;		// 객체의 정보출력
	};
}