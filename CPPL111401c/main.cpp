#include "AlarmClock.h"

using namespace CPPL111401c;

int main()
{
	Time alarm(6, 0, 0);
	Time current(12, 56, 34);
	AlarmClock c(alarm, current);

	c.print();
	return 0;
}
