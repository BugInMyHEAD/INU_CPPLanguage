#pragma once

namespace CPPL111401c
{
	class Time
	{
		int hr;		// 시간
		int min;		// 분
		int sec;		// 초

	public:

		Time(int hr = 0, int min = 0, int sec = 0)
			: hr(hr), min(min), sec(sec)
		{
		};

		void print() const;		// 객체의정보출력
	};
}