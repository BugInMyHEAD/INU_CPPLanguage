#include <string>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main()
{
	int width = 15;
	cout << left;
	cout << setw(15) << "Number";
	cout << setw(15) << "Number";
	cout << setw(15) << "Number";
	for (int i2 = 0; i2 < 10; i2++)
	{
		int i3 = i2 * 5;
		cout << setw(15) << setfill('_') << setprecision(3) << sqrt(i3);
	}
}