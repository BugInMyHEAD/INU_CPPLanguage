#pragma once

namespace CPPL111402c
{
	class Point
	{
		int x, y;

	public:

		Point(int x = 0, int y = 0)
			: x(x), y(y)
		{
		}

		void print() const;
	};
}