#pragma once

#include "Point.h"

namespace CPPL111402c
{
	class Circle
	{
		Point center;
		int radius;

	public:

		Circle(Point point = Point(0, 0), int radius = 0)
		{
		}

		Circle(int radius, int x = 0, int y = 0)
			: Circle(Point(x, y), 0)
		{
		}

		void print() const;
	};
}